# Analysis
- After reading this problem, the word 'swap' should give you a connection to bubblesort immediately, this is just a simple bubble sort implementation, just keep track of how many swaps it takes for each sort.

## Time Complexity

- Though $`N`$ is not given, $`L`$ is, and I can tell you that $`N`$ is **not an arbitrary** number, so lets just say $`N \le 50`$.     
- $`L`$ is guaranteed to be less than or equal to $`50`$, so the time complexity would be $`O(L^2 \times N)`$, or about $`O(N^3)`$   
- That makes this run in about $`O(50^3)`$ or $`O(75 000)`$, which will run in time   

