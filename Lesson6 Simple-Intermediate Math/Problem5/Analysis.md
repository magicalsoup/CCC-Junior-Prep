# Analysis
- Let $`t_1`$ be the time it takes for the stone to hit the bottom of the well. The remaining time taken by the sound to climb back up is $`t-t_1`$. 
- Let the depth of the well be $`x`$.
- You will get $`x = \frac{(gt_1)^2}{2}`$ and $`x = c(t-t_1)`$.
- Solving the equation for $`x`$ will give you the depth of the well.
- The velocity as the stone hits the water is $`gt_1`$.

## Time Complexity
- The time complexity will be $`O(1)`$ time and will pass the time limit.

## Data Types
- A double data type is sufficient.
