# Analysis
- Though we haven't really learned brute force yet, you can understand the problem this way:   
    - Just loop through all possible triples and find the ones where the median is equal to $`x`$    
- Go check out lesson 8 for more information on brute force.

## Time Complexity
 - Going through all possible triples is $`O(N^3)`$ time, as $`N`$ is only up to $`100`$, this will pass in time..
