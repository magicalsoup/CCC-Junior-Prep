# Analysis
- Its pretty straightforward, for each node, dfs all the nodes that are connected to it, if there is a cycle, output `N`, `Y` otherwise.

## Time Complexity
- $`O(N(V + E))`$, or $`O(10^4(20000)`$, which is about $`2 \times 10^8`$, which will run in time.
